# Tech demo
Tato ukázka slouží k demonstraci DevOps technologií. Vytvořeno pro bakalářský projekt "Vývoj aplikací s využitím DevOps".
## Použité technologie
- Gitlab, Gitlab CI/CD
- Docker
- Kubernetes
- Terraform
