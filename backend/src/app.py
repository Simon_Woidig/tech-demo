from flask import Flask, Response, make_response


def create_app() -> Flask:
    flask = Flask(__name__)

    @flask.route("/")
    def index() -> Response:
        res = make_response({"status": 200, "msg": "ok"}, 200)
        res.headers.add_header("Access-Control-Allow-Origin", "*")
        return res

    return flask


if __name__ == "__main__":  # nosec
    app = create_app()
    app.env = "development"
    app.run(host="0.0.0.0", debug=True)
