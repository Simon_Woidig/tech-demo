import * as dotenv from 'dotenv';
dotenv.config();

import express from 'express';
import nunjucks from 'nunjucks';
import { fetchDataFromApi } from './src/scripts.js';

const app = express();

nunjucks.configure('views', {
    autoescape: true,
    express: app
});

const appPort = process.env.APP_PORT;
const apiUrl = process.env.API_URL;

app.listen(appPort, () => {
    console.log(`Application started and Listening on port ${appPort}`);
});

app.get("/", function (req, res) {
    fetchDataFromApi(apiUrl, "")
        .then(data => res.render("./index.njk", data));
});