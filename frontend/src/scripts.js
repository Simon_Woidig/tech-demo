import fetch from "node-fetch";

async function fetchDataFromApi(apiUrl, endpoint) {
    try {
        let result = await fetch(`http://${apiUrl}/${endpoint}`);
        if (result.ok) {
            try {
                let jsonData = await result.json();
                return { "error": null, "data": { "status": jsonData.status, "msg": jsonData.msg } };
            } catch (err) {
                console.error("Encountered error:");
                console.error(err);
                return { "error": "JSON parse error", "data": null };
            }
        }
        return { "error": null, "data": { "status": result.status, "msg": "API error" } };
    } catch (err) {
        console.error("Encountered error:");
        console.error(err);
        return { "error": "API unreachable", "data": null };
    }
}

export { fetchDataFromApi };