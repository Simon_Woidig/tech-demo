terraform {
  required_version = ">=v1.1.9"
  backend "http" {}
}
variable "hcloud_token" {
  type      = string
  sensitive = true
}
module "node_docker" {
  source       = "./node_docker"
  hcloud_token = var.hcloud_token
}
module "node_kubernetes" {
  source       = "./node_kubernetes"
  hcloud_token = var.hcloud_token
}
module "node_podman" {
  source       = "./node_podman"
  hcloud_token = var.hcloud_token
}

output "node_docker_ip" {
  value = module.node_docker.node_ip
}
output "node_kubernetes_ip" {
  value = module.node_kubernetes.node_ip
}
output "node_podman_ip" {
  value = module.node_podman.node_ip
}
