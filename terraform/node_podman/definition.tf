terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.35.1"
    }
  }
}
variable "hcloud_token" {
  type      = string
  sensitive = true
}
provider "hcloud" {
  token = var.hcloud_token
}

data "hcloud_ssh_keys" "all_keys" {}
resource "hcloud_server" "node_podman" {
  name        = "node-podman"
  image       = "debian-11"
  server_type = "cpx31"
  location    = "fsn1"
  user_data   = file("files/podman-cloud-init.yaml")
  ssh_keys    = data.hcloud_ssh_keys.all_keys.ssh_keys.*.name
}
output "node_ip" {
  value = hcloud_server.node_podman.ipv4_address
}
